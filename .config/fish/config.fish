if status is-interactive
# Commands to run in interactive sessions can go here
end
# THEME PURE #
set fish_function_path /home/mike/.config/fish/functions/theme-pure/functions/ $fish_function_path
source /home/mike/.config/fish/functions/theme-pure/conf.d/pure.fish
#
# My customizations
alias dots='/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME'
#alias ll='ls -lhArt'
alias ls='exa'
alias ll='exa -lgh@ --git --sort=modified'
alias lla='exa -lgh@a --git --sort=modified'
alias sudo='sudo '
alias mc='/home/mike/.local/bin/mc-skin.sh'
alias view='vim -R'
