#!/bin/sh

let VOL=$(amixer get Master | egrep -o "[0-9]+%" | egrep -o "[0-9]*")

function power_status() {
    battstat=$(acpi -b 2> /dev/null | awk '/Battery 0/{print $4}' | cut -d, -f1 | tr -d '%')
#    powstat$(cat /sys/class/power_supply/AC/online)"
    if [ -x /sys/class/power_supply/BAT0 ] ; then
        if [ "$(cat /sys/class/power_supply/AC/online)" == 1 ];
        then
            if [ $battstat -lt 96 ];
            then
                printf "#[fg=brightyellow] 🗲#[fg=default] ${battstat}%% (Charging) | "
            else
                printf " "
            fi
        else
            remtime=$(acpi -b | awk '{printf " ("$5" remaining)"}')
            case $battstat in
                100|9[0-9]|8[0-9]|7[0-9]|6[0-9]|5[0-9]|4[6-9]) fgcolor='#[fg=white]'
                    ;;
                4[0-5]|3[0-9]|2[3-9]) fgcolor='#[fg=brightyellow]'
                    ;;
                2[0-2]|1[0-9]|[0-9]) fgcolor='#[fg=brightred]'
                    ;;
            esac
            printf "%s " "#[fg=yellow] 🗲 ${fgcolor}${battstat}%#[fg=#808080]${remtime}#[fg=default] |"
        fi
    fi
}



#if [ $(acpi | grep -c Discharging) -eq 1 ];
#then
#    PWR="🔋$(acpi | cut -d ',' -f 2 | sed 's/%//')%% |"
#else
#    PWR="🗲|"
#fi
TMP=$(acpi -t | awk '{print $4"°"}')
LDATE=$(date +'%a %d %b %H:%M')
UDATE=$(date -u +'(%H:%M UTC)')
#printf " $VOL |"
#printf "$PWR "
power_status
printf "${TMP}#[fg=#808080]C#[fg=white] | "
printf "#[fg=white]$LDATE "
printf "#[fg=#808080]$UDATE"

