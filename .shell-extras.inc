# vim:ft=bash:

[[ "$(command -v nvim)" ]] && export VI=$(printf "nvim") || export VI=$(printf "vim")

## Seet some options
export EDITOR="$VI"
export VISUAL="$VI"
export SYSTEMD_EDITOR="$VI"
export VIRSH_DEFAULT_CONNECT_URI='qemu:///system'

## Set paths for Texlive
if [[ $(uname -v) != *"NixOS"* ]];
then
    export MANPATH="$MANPATH:$($(which ls) -d1 /usr/local/texlive/*/texmf-dist/doc/man | tail -1)"
    export INFOPATH="$INFOPATH:$($(which ls) -d1 /usr/local/texlive/*/texmf-dist/doc/info | tail -1)"
    export PATH="$PATH:$($(which ls) -d1 /usr/local/texlive/*/bin/x86_64-linux | tail -1)"
fi

## Alias git command to dots to work with your dotfiles repository
#alias dots='/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME'

git() {
    if [ "$PWD" = "$HOME" ]; then
        command git --git-dir="$HOME/.dotfiles.git" --work-tree="$HOME" "$@"
    else
        command git "$@"
    fi
}

## Sometimes I want a new/seperate window for the editor
gvim() {
    kitty --detach sh -c "$VI $@" -T "$@"
}

## Aliases for shell
alias sudo='sudo '
alias view='$VI -R'
alias mc='/home/mike/.local/bin/mc-skin.sh'
alias btm='btm --color gruvbox -m'

## Alias 'ls' command to 'eza' command
alias ell='eza -lh@ --git --sort=modified'
alias ela='eza -lh@a --git --sort=modified'
alias ls='ls --group-directories-first --color=auto -F -v'
alias ll='ls -lh'
alias lla='ls -lAh'
alias l.='ls -d .*'

passgen () {
    printf "\n~~ Password Generator ~~\n\n"
    read -p "Enter the password lenght: " PASS_LENGTH
    # how many example passwords to generate
    GenCount=5
    for VAR in $(seq 1 $GenCount);
    do
        openssl rand -base64 48 | cut -c1-$PASS_LENGTH
        #-base64(Encode) 48 is length
        #cut is for user input column -c1 is column1
    done
}

## Randomize lines in a file
mixitup () {
    if [[ -z "$@" ]];
    then
        printf "\nFunction to randomize rows of a given file (think an m3u playlist).\n"
        printf "Outputs to stdout, so you may want to re-direct to a new file.\n"
        printf "\nExamples:\n"
        printf "    mixitup playlist.m3u > randomized-playlist.m3u\n"
        printf "    mixitup playlist1.m3u playlist2.m3u playlist3.m3u > randomized-playlist.m3u\n"
    fi
    if [[ shuf ]];
    then
        MIX='shuf'
    else
        MIX="awk 'BEGIN{srand()}{print rand(), $0}' $1 | sort -n -k 1 | awk 'sub(/\S* /,\"\")'"
    fi
    for PL in "$@";
    do
        $MIX "${PL}"
    done
}

## Add personal bin to PATH
export PATH=$PATH:~/.local/bin

[[ -f ~/.bash-tmux.sh ]] && . ~/.bash-tmux.sh

## Add alias for KiTTY terminal
if [[ $TERM =~ "kitty" ]];
then
    alias ssh="kitty +kitten ssh"
fi

## For use with using GnuGP for SSH keys
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent

