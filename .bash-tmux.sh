#!/bin/bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && exit

function ToTmuxOrNotToTmux {
    SESSNAME='One'

    IS_SESSION=$(tmux ls 2>/dev/null | grep -q "${SESSNAME}:"; echo $?)
    IS_ATTACHED=$(tmux ls 2>/dev/null | grep "${SESSNAME}:" | grep -q "attached"; echo $?)

    if [[ $IS_SESSION -eq 1 ]];
    then
        TMUXSESSION=new
        MSG='Would you like to launch a TMUX session'
    elif [[ $IS_SESSION -eq 0 ]] && [[ $IS_ATTACHED -gt 0 ]];
    then
        TMUXSESSION=join
        MSG='Would you like to connect to the existing TMUX'
    fi
}

TMUX_ATTACHED=$(tmux ls 2>/dev/null | grep "${SESSNAME}:" | grep -c "attached")
if [ "$TMUX_ATTACHED" -lt '1' ];
then
    if [ $(echo "$TERM" | grep -c tmux) -lt '1' ];
    then
        if [ -f $HOME/.bash-tmux.sh ];
        then
            ToTmuxOrNotToTmux
#             printf "\n + + + Call Script Here + + +\n\n"
    printf "\n      ● ● ●  ${MSG} (y/n)?  ● ● ●\n"
    read -n 1 TMUXYN
    printf "\b \n"
    if [ "$TMUXYN" = 'Y' ] || [ "$TMUXYN" = 'y' ];
    then
        case "$TMUXSESSION" in
            "new")
                TMUX='' tmux new-session -s "${SESSNAME}" -c ~ fish ';' neww
                ;;
            "join")
                tmux attach-session -t "${SESSNAME}"
                ;;
        esac
    fi
        fi
    fi
fi

unset TMUXSESSION TMUXYN IS_SESSION IS_ATTACHED TMUX_ATTACHED

