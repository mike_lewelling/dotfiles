" Setup tabline
  """ This is a moderately modified version of the plugin https://github.com/mkitt/tabline.vim
hi TabLine      ctermfg=Black  ctermbg=Green     cterm=NONE
hi TabLineFill  ctermfg=Black  ctermbg=Green     cterm=NONE
hi TabLineSel   ctermfg=White  ctermbg=DarkBlue  cterm=NONE
function! Tabline()
  let s = ''
  for i in range(tabpagenr('$'))
    let tab = i + 1
    let winnr = tabpagewinnr(tab)
    let buflist = tabpagebuflist(tab)
    let bufnr = buflist[winnr - 1]
    let bufname = bufname(bufnr)
    let bufmodified = getbufvar(bufnr, "&mod")

    let s .= '%' . tab . 'T'
    let s .= (tab == tabpagenr() ? '%#TabLineSel#' : '%#TabLine#')
    let s .= ' ' . tab .''
    if winnr > 1
      let s .= (bufname != '' ? ' (' . (winnr) . '): ' . fnamemodify(bufname, ':t') . ' ' : '[No Name] ')
    else
      let s .= (bufname != '' ? ': ' . fnamemodify(bufname, ':t') . ' ' : '[No Name] ')
    endif
    if bufmodified
      let s .= '+ '
    endif
  endfor

  let s .= '%#TabLineFill#'
  if (exists("g:tablineclosebutton"))
    let s .= '%=%999X⛒'
  endif
  return s
endfunction
set tabline=%!Tabline()
"


