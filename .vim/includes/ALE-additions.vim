" General config

let g:enable_bold_font = 1
let g:enable_italic_font = 1
let g:hybrid_transparent_background = 1
let g:ale_lint_on_enter = 1
let g:ale_lint_on_save = 1
let g:ale_sign_error = '●'
"let g:ale_sign_warning = '►'
let g:ale_sign_warning = '⚬'

" -----  Linters, Fixers and RLSs  ---------------
"
let g:ale_fixers = {
    \   'rust':    ['rustfmt', 'trim_whitespace', 'remove_trailing_lines'],
    \   'python':  ['yapf'],
    \ }

let g:ale_linters = {
    \   'c':         ['clang'],
    \   'cpp':       ['clang'],
    \   'vim':       ['vint'],
    \   'rust':      ['analyzer'],
    \   'python':    ['flake8', 'pylint'],
    \ }

let g:LanguageClient_serverCommands = {
    \ 'rust': ['rust-analyzer'],
    \ }

let g:ale_fix_on_save = 1
let g:ale_completion_enabled = 1
let g:ale_rust_cargo_use_check = 1
let g:ale_rust_cargo_check_tests = 1
let g:ale_rust_cargo_check_examples = 1

let $RUST_BACKTRACE = 1
let g:LanguageClient_loggingLevel = 'INFO'
let g:LanguageClient_virtualTextPrefix = ''
let g:LanguageClient_loggingFile =  expand('~/.vim/LanguageClient.log')
let g:LanguageClient_serverStderr = expand('~/.vim/LanguageServer.log')

