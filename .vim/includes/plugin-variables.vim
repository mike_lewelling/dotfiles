
" Plugin Variables/Settings

" Syntax entries for Clojure
"    let g:vimclojure#HighlightBuiltins = 1
"    let g:vimclojure#ParenRainbow = 1

" NERDTree
    let NERDTreeMinimalUI = 1
    let NERDTreeDirArrows = 1
    let g:NERDTreeClose='<F3>'
    let g:NERDTreeMapPreview='<F4>'
    let NERDTreeQuitOnOpen = 1
    let NERDTreeAutoDeleteBuffer = 1
    "NerdTree-git-plugin
    let g:NERDTreeGitStatusIndicatorMapCustom = {
        \ 'Modified'  : '✹',
        \ 'Staged'    : '✚',
        \ 'Untracked' : '✭',
        \ 'Renamed'   : '➜',
        \ 'Unmerged'  : '═',
        \ 'Deleted'   : '✖',
        \ 'Dirty'     : '✗',
        \ 'Clean'     : '✔︎',
        \ 'Ignored'   : '☒',
        \ 'Unknown'   : '?'
        \ }

" Netrw
    let g:netrw_localcopydircmd = 'cp -r'
    hi! link netrwMarkFile Search
    let g:netrw_browse_split = 3
    let g:netrw_liststyle = 3
    let g:netrw_winsize = -28
    let g:netrw_keepdir = 0
    let g:netrw_banner = 0
    let g:netrw_altv = 1
    set autochdir

" bling search highlight for current position
    let g:bling_time = 90
    let g:bling_count = 3

" Options for UltiSnip
    let g:UltiSnipsExpandTrigger = '<tab>'
    let g:UltiSnipsJumpForwardTrigger = '<tab>'
    let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
    " If you want :UltiSnipsEdit to split your window.
    let g:UltiSnipsEditSplit='normal'
"    let g:UltiSnipsSnippetDirectories =

" Set options for vimtex
    let g:tex_flavor='latex'
    let g:vimtex_view_general_viewer = 'okular'
    let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
    "let g:vimtex_quickfix_mode=0
    set conceallevel=2
    let g:tex_conceal='abdmg'
    hi clear conceal

" TerraForm
    let g:terraform_align=1
    let g:terraform_fold_sections=1
    let g:terraform_fmt_on_save=1

" tagbar
  let g:tagbar_type_rust = {
    \ 'ctagstype' : 'rust',
    \ 'kinds' : [
        \'T:types,type definitions',
        \'f:functions,function definitions',
        \'g:enum,enumeration names',
        \'s:structure names',
        \'m:modules,module names',
        \'c:consts,static constants',
        \'t:traits',
        \'i:impls,trait implementations',
    \]
    \}

let g:indentLine_char_list = ['┊', '|', '¦', '┆']

if !exists('g:undotree_WindowLayout')
   let g:undotree_WindowLayout = 2
   let g:undotree_SetFocusWhenToggle = 1
endif

" Write all buffers before navigating from Vim to tmux pane
 let g:tmux_navigator_save_on_switch = 2

" Rainbow Parentheses
let g:rainbow_active = 1

" Enable "GitLens" feature
if !has('nvim')
    let g:GIT_LENS_ENABLED = 1
else
    let g:blamer_enabled = 1
    let g:blamer_prefix = '    '
endif

" Call thesaurus
nnoremap <leader>t :ThesaurusQueryReplaceCurrentWord<CR>

" LanguageTool - for spelling and grammar checking
" Download: https://languagetool.org/download/LanguageTool-stable.zip
"let g:languagetool_jar='~/.vim/LanguageTool-6.1/languagetool-commandline.jar'
"let g:languagetool_win_height = 18
"nnoremap <leader>ltc :LanguageToolCheck<CR>
"nnoremap <leader>ltcl :LanguageToolClear<CR>

