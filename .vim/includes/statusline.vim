" Vim Statusline
" Original concept taken from https://www.reddit.com/r/vim/comments/6b7b08/my_custom_statusline/
"
if has('statusline')
set laststatus=2
set statusline=%!ActiveStatus()
autocmd Filetype qf setlocal statusline=%!InactiveStatus()

augroup status
  autocmd!
  autocmd WinEnter * setlocal statusline=%!ActiveStatus()
  autocmd WinLeave * setlocal statusline=%!InactiveStatus()
  autocmd ColorScheme * if(&background=="dark") | hi User1 guibg=#080808 guifg=#bcbcbc ctermbg=232 ctermfg=250 cterm=bold | endif
  autocmd ColorScheme * if(&background=="dark") | hi User2 guibg=#5f87d7 guifg=#080808 ctermbg=68 ctermfg=232 | endif
  autocmd ColorScheme * if(&background=="dark") | hi User3 guibg=#1c1c1c guifg=#5f87d7 ctermbg=234 ctermfg=68 | endif
  autocmd ColorScheme * if(&background=="dark") | hi User4 guibg=#1c1c1c guifg=#dadada ctermbg=234 ctermfg=254 | endif
  autocmd ColorScheme * if(&background=="dark") | hi User5 guibg=#2e4053 guifg=#bcbcbc ctermbg=236 ctermfg=250 | endif
  autocmd ColorScheme * if(&background=="dark") | hi User6 guibg=#ff5f00 guifg=#dadada ctermbg=202 ctermfg=253 | endif
  autocmd ColorScheme * if(&background=="dark") | hi User7 guibg=#17202a guifg=#ff5f00 ctermbg=234 ctermfg=202 | endif
  autocmd ColorScheme * if(&background=="dark") | hi User8 guibg=#1c1c1c guifg=Red ctermbg=234 ctermfg=red | endif
  autocmd ColorScheme * if(&background=="dark") | hi User9 guibg=#1c1c1c guifg=#ffaf00 ctermbg=234 ctermfg=214 | endif
" Mode changes
au InsertEnter * if(&background=="dark") | hi User2 guibg=Red guifg=#17202a ctermbg=Red ctermfg=234 | hi User3 guibg=#17202a guifg=Red ctermbg=234 ctermfg=Red | endif
au InsertLeave * if(&background=="dark") | hi User2 guibg=#5f87d7 guifg=#1c1c1c ctermbg=68  ctermfg=234 | hi User3 guibg=#1c1c1c guifg=#5f87d7 ctermbg=234 ctermfg=68 | endif
" Set filetype to display in italic and slightly fainter
autocmd ColorScheme * if(&background=="dark") | hi MyFt guibg=#1c1c1c guifg=#637777 ctermbg=234 ctermfg=243 gui=italic cterm=italic | endif
"
  autocmd ColorScheme * if(&background=="light") | hi User1 guibg=#d0d0d0 guifg=#000000 ctermbg=252 ctermfg=000 | endif
  autocmd ColorScheme * if(&background=="light") | hi User2 guibg=#ffaf00 guifg=#d0d0d0 ctermbg=214 ctermfg=252 | endif
  autocmd ColorScheme * if(&background=="light") | hi User3 guibg=#e4e4e4 guifg=#ffaf00 ctermbg=254 ctermfg=214 | endif
  autocmd ColorScheme * if(&background=="light") | hi User4 guibg=#e4e4e4 guifg=#808080 ctermbg=254 ctermfg=244 gui=bold cterm=bold | endif
  autocmd ColorScheme * if(&background=="light") | hi User5 guibg=#d7d7af guifg=#585858 ctermbg=187 ctermfg=240 | endif
  autocmd ColorScheme * if(&background=="light") | hi User6 guibg=#ff5f00 guifg=#d0d0d0 ctermbg=202 ctermfg=252 | endif
  autocmd ColorScheme * if(&background=="light") | hi User7 guibg=#d0d0d0 guifg=#ff5f00 ctermbg=252 ctermfg=202 | endif
  autocmd ColorScheme * if(&background=="light") | hi User8 guibg=#e4e4e4 guifg=Red ctermbg=254 ctermfg=red gui=bold cterm=bold | endif
  autocmd ColorScheme * if(&background=="light") | hi User9 guibg=#e4e4e4 guifg=#5f87d7 ctermbg=254 ctermfg=68 gui=bold cterm=bold | endif
" Mode changes
au InsertEnter * if(&background=="light") | hi User2 guibg=Red guifg=#d0d0d0 ctermbg=Red ctermfg=252 | hi User3 guibg=#e4e4e4 guifg=Red ctermbg=254 ctermfg=Red | endif
au InsertLeave * if(&background=="light") | hi User2 guibg=#ffaf00 guifg=#d0d0d0 ctermbg=214 ctermfg=252 | hi User3 guibg=#e4e4e4 guifg=#ffaf00 ctermbg=254 ctermfg=214 | endif
" Set filetype to display in italic and slightly fainter
autocmd ColorScheme * if(&background=="light") | hi MyFt guibg=#e4e4e4 guifg=#a8a8a8 ctermbg=254 ctermfg=248 gui=bold,italic cterm=bold,italic | endif
augroup END


function! ActiveStatus()
  let statusline=""
  let statusline.="%1*"
  let statusline.="\ %{g:currentmode[mode()]}"
  let statusline.="%2*"
  let statusline.=""
  let statusline.="%5*"
  let statusline.="%{FugitiveHead()!=''?'\ \ '.FugitiveHead().'\ ':''}"
  let statusline.="%{'help'==&filetype?'\ Help\ ':''}"
  let statusline.="%{&previewwindow?'\ Preview Window\ ':''}"
  let statusline.="%3*"
  let statusline.=""
  let statusline.="%4*"
  let statusline.="\ %<"
  let statusline.="%{'help'!=&filetype?'\ \ '.Getfn().'\ ':''}"
  let statusline.="%8*"
  let statusline.="%{'help'!=&filetype?'\'.(&modified?'+\ ':'').(&readonly?'\ RO\ ':''):''}"
  let statusline.="%9*"
  let statusline.="%{80>winwidth(0)?'':'\ \ '.('help'!=&filetype?(ReadableBytes(getfsize(expand(@%)))).'\ ':'')}"
  let statusline.="%4*"
  let statusline.="%="
  let statusline.="%{80>winwidth(0)?'\ \ ':''}"
  let statusline.="%9*"
  let statusline.="\ %l,"                                    " Cursor row number
  let statusline.="%c%V"                                      " Cursor column and virt. column
  let statusline.="%4*"
  let statusline.="%{80>winwidth(0)?'':(&previewwindow?'':('help'!=&filetype?'\ \ ['.bufnr('%').'/'.bufnr('$').']':''))}"
  let statusline.="%9*"
  let statusline.="%{80>winwidth(0)?'':'\ \ '.('help'!=&filetype?'\ ts:'.&ts:'')}"
  let statusline.="%{80>winwidth(0)?'':'\ \ '.('text'==&filetype?'\ wc:'.(WordCount()).'':'')}"
  let statusline.="%{80>winwidth(0)?'':'\ \ '.('tex'==&filetype?'\ wc:'.(TexWordCount()).'':'')}"
  let statusline.="%4*"
  let statusline.="%{80>winwidth(0)?'\ \ ':'\ \ \ \ \ \ \ \ '}"
  let statusline.="%#MyFt#%{80>winwidth(0)?'':('help'!=&filetype?(strlen(&ft)?&ft.'\ \ ':'[filetype\ unknown]\ \ '):'')}"
  let statusline.="%3*"
  let statusline.=""
  let statusline.="%5*"
  let statusline.="%{(80>winwidth(0)?'':(&bomb\|\|'^$\|utf-8'!~#&fileencoding?'\ '.&fileencoding.(&bomb?'-BOM':'').'\ ':'').('utf-8'!~#&fileencoding?('unix'!=#&fileformat?'|\ '.&fileformat.'\ ':''):('unix'!=#&fileformat?'\ '.&fileformat.'\ ':'')))}"
"  let statusline.="\ %{(&fenc!=''?&fenc:&enc)}"
"  let statusline.="%{&bomb?'-BOM':''}"
"  let statusline.="\ "
"  let statusline.="\ %{&ff}\ "
"  let statusline.="\ \ "
  let statusline.="%2*"
  let statusline.=""
  let statusline.="%1*"
  let statusline.="\ %2p%%\ "
  let statusline.="%7*"
  let statusline.=""
  let statusline.="%6*%{LinterStatus()}"        " linter (currently Ale)
  return statusline
endfunction

function! InactiveStatus()
  let statusline=""
"  let statusline.="\ %{g:currentmode[mode()]}"
  let statusline.="\ "
  let statusline.=""
  let statusline.="%{FugitiveHead()!=''?'\ \ '.FugitiveHead().'\ ':''}"
  let statusline.="%{'help'==&filetype?'\ Help\ ':''}"
  let statusline.="%{&previewwindow?'\ Preview Window\ ':''}"
  let statusline.=""
  let statusline.="\ %<"
  let statusline.="%{'help'!=&filetype?'\ \ '.Getfn().'\ ':''}"
  let statusline.="%{'qf'==&filetype?'\ Quickfix List\ ':''}"
  let statusline.="%{'help'!=&filetype?'\ '.(&modified?'+\ ':'').(&readonly?'\ RO\ ':''):''}"
  let statusline.="%="
  let statusline.="%l,"                                    " Cursor row number
  let statusline.="%c%V"                                      " Cursor column and virt. column
  let statusline.="%{80>winwidth(0)?'\ \ ':'\ \ \ \ \ '}"
  let statusline.="%{80>winwidth(0)?'':(&previewwindow?'':('help'!=&filetype?'\ \ ['.bufnr('%').'/'.bufnr('$').']':''))}"
  let statusline.="%{80>winwidth(0)?'\ \ ':'\ \ \ \ \ \ \ \ \ \ '}"
  let statusline.="%{80>winwidth(0)?'':('help'!=&filetype?(strlen(&ft)?&ft.'\ \ ':'[filetype\ unknown]\ \ '):'')}"
  let statusline.=""
  let statusline.="%{(80>winwidth(0)?'':(&bomb\|\|'^$\|utf-8'!~#&fileencoding?'\ '.&fileencoding.(&bomb?'-BOM':'').'\ ':'').('utf-8'!~#&fileencoding?('unix'!=#&fileformat?'|\ '.&fileformat.'\ ':''):('unix'!=#&fileformat?'\ '.&fileformat.'\ ':'')))}"
  let statusline.=""
  let statusline.="\ "
"  let statusline.=""
"  let statusline.="%6*%{LinterStatus()}"        " linter (currently Ale)
  return statusline
endfunction

" -----  Ale Errors/Warnings  -------------------------------------------
function! LinterStatus() abort
    let l:counts = ale#statusline#Count(bufnr(''))

    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors

    return l:counts.total == 0 ? '' : printf(
        \   '  W:%d  E:%d  ',
        \   all_non_errors,
        \   all_errors
        \)
endfunction

let g:currentmode={
    \ 'n'      : 'N ',
    \ 'no'     : 'N·Operator Pending ',
    \ 'v'      : 'V ',
    \ 'V'      : 'V·Line ',
    \ ''     : 'V·Block ',
    \ 's'      : 'Select ',
    \ 'S'      : 'S·Line ',
    \ ''     : 'S·Block ',
    \ 'i'      : 'I ',
    \ 'R'      : 'R ',
    \ 'Rv'     : 'V·Replace ',
    \ 'c'      : 'Command ',
    \ 'cv'     : 'Vim Ex ',
    \ 'ce'     : 'Ex ',
    \ 'r'      : 'Prompt ',
    \ 'rm'     : 'More ',
    \ 'r?'     : 'Confirm ',
    \ '!'      : 'Shell ',
    \ 't'      : 'Terminal '
    \}

function Getfn()
    if &previewwindow
        return ''
    else
        return expand('%:f')
    endif
endfunction


function ReadableBytes(bytes)
    if a:bytes < 1
        return "new"
        exit
    endif
    let i = float2nr(floor(log(a:bytes) / log(1024)))
    let sizes=['B', 'K', 'M', 'G']
    let fsize=(a:bytes / pow(1024, i))
    return printf('%.1f%s', fsize, sizes[i])
endfunction


let g:word_count=wordcount().words
function WordCount()
    if has_key(wordcount(),'visual_words')
        let g:word_count=wordcount().visual_words."/".wordcount().words " count selected words
    else
"        let g:word_count=wordcount().cursor_words."/".wordcount().words " or shows words 'so far'
        let g:word_count=wordcount().words   " or shows words 'so far'
    endif
    return g:word_count
endfunction


function! TexWordCount()
    let filename = expand("%")
"    let cmd = "detex " . filename . " | wc -w | tr -d '[:space:]'"
    let cmd = "perl ~/.vim/includes/latexcount.pl " . filename . " | grep total | awk '{print $1}' | tr -d '[:space:]'"
    let result = system(cmd)
    return result
endfunction



endif "has('statusline')

