
" Functions

" -----  As it reads  ------------------------------------------------
function! StripTrailingWhitespace()
    """ Remove trailing whitespace
    """ Only strip if the b:noStripeWhitespace variable isn't set
    if exists('b:noStripWhitespace')
        return
    endif
   autocmd BufWritePre <buffer> :%s/\s\+$//e
endfunction "StripTrailingWhitespace()


" -----  Spell Checking  ---------------------------------------------
function! SpellToggle()
    """ spell check - Word Suggestions
    if &spell == "0"
        hi SpellBad cterm=underline ctermfg=9 guifg=#ff0000
        hi SpellLocal cterm=underline ctermfg=178 guifg=#d7af00
        hi SpellRare cterm=underline ctermfg=107 guifg=#87af5f
        hi SpellCap cterm=underline ctermfg=75 guifg=#5fafff
        setlocal spell
        echo " Spell-check enabled"
    else
        setlocal nospell
        echo " Spell-check disabled"
    endif
endfunction "SpellToggle()


" -----  As it reads  ------------------------------------------------
function! ToggleMouse()
    """ Turn off mouse and numbering so copy and paste is not a hassle
    if &mouse == "a"
        set mouse=""
        set paste
        set nonumber
        set norelativenumber
        echo "Mouse Cut-n-Paste mode: Enabled"
    else
        set mouse=a
        set nopaste
        set number
        set relativenumber
        echo "Mouse Cut-n-Paste mode: Deactivate"
    endif
endfunction "ToggleMouse()


" -----  Toggle File Browser  ----------------------------------------
function! ToggleVExplorer()
    if exists("t:expl_buf_num")
        let expl_win_num = bufwinnr(t:expl_buf_num)
        let cur_win_num = winnr()
        let g:netrw_liststyle = 3
        let g:netrw_browse_split = 4

        if expl_win_num != -1
            while expl_win_num != cur_win_num
                exec "wincmd w"
                let cur_win_num = winnr()
                autocmd filetype netrw nmap <c-a> <cr>:wincmd W<cr>
            endwhile
            close
        endif
        unlet t:expl_buf_num
    else
         Vexplore
         let t:expl_buf_num = bufnr("%")
    endif
endfunction


" -----  Colorscheme Changer  ---------------------------------------
" Change the color scheme from a list of color scheme names.
" Moderately modified to serve my own needs.
" from http://vim.wikia.com/wiki/VimTip341

" If already loaded, we're done...
if exists("loaded_SetColors")
    finish
endif
let loaded_SetColors = 1

function! SetColorDark()
    if has('gui_running') || (&termguicolors == '1')
        let s:mycolors = ['lucius', 'two-firewatch', 'corporation', 'night-owl', 'habamax']
      else
        let s:mycolors = ['lucius', 'apprentice', 'night-owl', 'twilight256', 'iceberg']
    endif
    let current = index(s:mycolors, g:colors_name)
  let missing = []
  for i in range(len(s:mycolors))
    let current += 1
      if !(0 <= current && current < len(s:mycolors))
        let current = (1>0 ? 0 : len(s:mycolors)-1)
      endif
    try
      execute 'set background=dark'
      execute 'colorscheme '.s:mycolors[current]
      break
    catch /E185:/
      call add(missing, s:mycolors[current])
    endtry
  endfor
  redraw
  if len(missing) > 0
    echo 'Error: colorscheme not found:' join(missing)
  endif
  echo g:colors_name
"    "Highlight text that run past the 80th column
"        highlight OverLength ctermfg=red guifg=red
"        match OverLength /\%>80v.\+/
endfunction

" -----  Now the light schemes  -------------------------------------
function! SetColorLight()
    if has('gui_running') || (&termguicolors == '1')
        let s:mycolors = ['lucius', 'iceberg']
      else"
        let s:mycolors = ['lucius', 'iceberg']
    endif
    let current = index(s:mycolors, g:colors_name)
  let missing = []
  for i in range(len(s:mycolors))
    let current += 1
      if !(0 <= current && current < len(s:mycolors))
        let current = (1>0 ? 0 : len(s:mycolors)-1)
      endif
    try
      execute 'set background=light'
      execute 'colorscheme '.s:mycolors[current]
      break
    catch /E185:/
      call add(missing, s:mycolors[current])
    endtry
  endfor
  redraw
  if len(missing) > 0
    echo 'Error: colorscheme not found:' join(missing)
  endif
  echo g:colors_name
"    "Highlight text that run past the 80th column
"        highlight OverLength ctermfg=red guifg=red
 "       match OverLength /\%>80v.\+/
endfunction

" -----  Highlight Duplicate lines  ---------------------------------
function! HlDs()
    syn clear Repeat | g/^\(.*\)\n\ze\%(.*\n\)*\1$/exe 'syn match Repeat "^' . escape(getline('.'), '".\^$*[]') . '$"' | nohlsearch
endfunction

