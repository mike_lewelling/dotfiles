" Set mapleader
let mapleader = ","
let g:mapleader = ","

" Clear highlighted search until next search
nmap <silent><leader>/ :noh<CR>
nmap <silent><esc> <cmd>noh<cr><esc>

" <F2> Turn off mouse and line numbering so copy and paste is not a hassle
nnoremap <silent><F2> :call ToggleMouse()<CR>

" <S-F2> Highlight Duplicate lines
nnoremap <silent><S-F2> :call HlDs()<CR>

" <F3> Toggle NerdTree or Netrw/Explore
nnoremap <silent><F3> :NERDTreeToggle<CR>
"    nnoremap <silent><F3> :call ToggleVExplorer()<CR>

" <F5> Toggle UndoTree
noremap <F5> :UndotreeToggle<CR>

" <S-F5> Toggle for Tagbar
nnoremap <S-F5> :TagbarToggle<CR>

" <F7> for vim built-in spelling check/suggestions
nnoremap <F7> :call SpellToggle()<CR>

" <F8> for thesaurus <S-F8> to pass selected phrase to thesaurus
nnoremap <F8> :ThesaurusQueryReplaceCurrentWord<CR>   " also <leader>cs

" <F9> Toggle for light color schemes
nnoremap <silent><F9> :call SetColorLight()<CR>
" <F10> Toggle for dark color schemes
nnoremap <silent><F10> :call SetColorDark()<CR>

" <F12> Display my keymap listings  " Try doing this with a global mark instead  ie. :'K
nnoremap <F12> :80vsp $HOME/.vim/doc/my-key-maps.txt<CR>

" <S-F12> Take screenshot - Make a copy of the current file source in HTML
noremap <silent><S-F12> :TOhtml<cr>
let g:html_prevent_copy = "n"


" Tabularize plugin   --- You REALLY need to customize these
if exists(":Tabularize")
nmap <Leader>a= :Tabularize /=<CR>
vmap <Leader>a= :Tabularize /=<CR>
nmap <Leader>a/| :Tabularize /|<CR>
vmap <Leader>a/| :Tabularize /|<CR>
nmap <Leader>a: :Tabularize /:<CR>
nmap <Leader>a: :Tabularize /:<CR>
vmap <Leader>a: :Tabularize /:<CR>
nmap <Leader>a:: :Tabularize /:\zs<CR>
vmap <Leader>a:: :Tabularize /:\zs<CR>
nmap <Leader>a, :Tabularize /,<CR>
vmap <Leader>a, :Tabularize /,<CR>
nmap <Leader>a<Bar> :Tabularize /<Bar><CR>
vmap <Leader>a<Bar> :Tabularize /<Bar><CR>
endif

" Swap v and CTRL-v, because Block mode is more useful that Visual mode
nnoremap    v   <C-V>
nnoremap <C-V>     v
vnoremap    v   <C-V>
vnoremap <C-V>     v

" Wrapped lines goes down/up to next row, rather than next line in file.
nnoremap j gj
nnoremap k gk

" Simplify split navigation
nnoremap <C-j> <C-W><C-J>
nnoremap <C-k> <C-W><C-K>
nnoremap <C-l> <C-W><C-L>
nnoremap <C-h> <C-W><C-H>
