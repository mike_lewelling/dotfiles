#!/bin/sh

mkdir -p ~/.vim/{misc/{backups,swap,tags,undo,views},colors,includes,spell,thesaurus,test,pack/{colorschemes,plugins,test}/{start,opt}};
#mkdir -p ~/.vim/{misc/{backups,swap,tags,undo,views},colors,includes,spell,thesaurus,test,pack/{colorschemes,function,code,syntax,text,test}/{start,opt};
if [ $? -gt '0' ];
then
    printf "\n   PROGRAM ABORTED\n\n"
    exit 1
fi

BUNDLES=( \
    'https://github.com/cespare/vim-toml.git' \
    'https://github.com/christoomey/vim-tmux-navigator.git' \
    'https://github.com/dense-analysis/ale.git' \
    'https://github.com/dpelle/vim-LanguageTool.git' \
    'https://github.com/ekalinin/Dockerfile.vim.git' \
    'https://github.com/gabrielelana/vim-markdown.git' \
    'https://github.com/godlygeek/tabular.git' \
    'https://github.com/hashivim/vim-terraform.git' \
    'https://github.com/honza/vim-snippets.git'
    'https://github.com/ivyl/vim-bling' \
    'https://github.com/juliosueiras/vim-terraform-completion.git' \
    'https://github.com/lervag/vimtex.git' \
    'https://github.com/luochen1990/rainbow.git' \
    'https://github.com/mbbill/undotree.git' \
    'https://github.com/mhinz/vim-signify.git' \
    'https://github.com/preservim/nerdtree.git' \
    'https://github.com/preservim/tagbar.git' \
    'https://github.com/rbtnn/vim-powershell' \
    'https://github.com/SirVer/ultisnips.git' \
    'https://github.com/Townk/vim-autoclose.git' \
    'https://github.com/tpope/vim-fugitive.git' \
    'https://github.com/vim-scripts/AutoComplPop.git'
    'https://github.com/vim-scripts/csv.vim.git' \
    'https://github.com/vim-scripts/ScreenShot.git' \
    'https://github.com/Xuyuanp/nerdtree-git-plugin.git' \
    'https://github.com/Yggdroot/indentLine.git' \
    'https://github.com/mhinz/vim-startify.git' \
    'https://github.com/serayuzgur/crates.git' \
    'https://github.com/Eliot00/git-lens.vim.git' \
    'https://github.com/LnL7/vim-nix.git' \
    'https://github.com/vmchale/ion-vim.git' \
    'https://github.com/rust-lang/rust.vim.git' \
    'https://github.com/dhruvasagar/vim-table-mode.git' \
    'https://github.com/Ron89/thesaurus_query.vim.git'
)

#declare -A PLUGTYPE=(
#[TEXT]="https://github.com/dpelle/vim-LanguageTool.git https://github.com/Ron89/thesaurus_query.vim.git https://github.com/dhruvasagar/vim-table-mode.git"
#[CODE]="https://github.com/dense-analysis/ale.git https://github.com/serayuzgur/crates.git https://github.com/honza/vim-snippets.git https://github.com/preservim/tagbar.git https://github.com/SirVer/ultisnips.git"
#[FUNCTION]="https://github.com/christoomey/vim-tmux-navigator.git https://github.com/Eliot00/git-lens.vim.git https://github.com/godlygeek/tabular.git https://github.com/ivyl/vim-bling https://github.com/luochen1990/rainbow.git https://github.com/mbbill/undotree.git https://github.com/mhinz/vim-signify.git https://github.com/mhinz/vim-startify.git https://github.com/preservim/nerdtree.git https://github.com/Townk/vim-autoclose.git https://github.com/vim-scripts/ScreenShot.git https://github.com/Xuyuanp/nerdtree-git-plugin.git https://github.com/Yggdroot/indentLine.git"
#[SYNTAX]="https://github.com/cespare/vim-toml.git https://github.com/ekalinin/Dockerfile.vim.git https://github.com/gabrielelana/vim-markdown.git https://github.com/hashivim/vim-terraform.git https://github.com/juliosueiras/vim-terraform-completion.git https://github.com/lervag/vimtex.git https://github.com/rbtnn/vim-powershell https://github.com/tpope/vim-fugitive.git https://github.com/vim-scripts/AutoComplPop.git https://github.com/vim-scripts/csv.vim.git"
#[COLORSCHEMES]= "")
#
#for PT in ${!PLUGTYPE[@]};
#do
#    pt=$(echo $PT | tr '[:upper:]' '[:lower:]')
#    printf "\nInstalling the following plugins to $pt...\n"
#
#    if [ "$pt" == "function" ]
#    then
#        P2="start"
#    else
#        P2="opt"
#    fi
#    cd ~/.vim/pack/${pt}/${P2}
#
#    for pkg in ${PLUGTYPE[$PT]};
#    do
#        printf "git clone $pkg ..."
#        git clone $pkg 1>/dev/null 2>&1
#        if [ $? -gt '0' ];
#        then
#            printf " FAIL\n"
#        else
#            printf " Success\n"
#        fi
#    done
#done

printf "\n\nInstalling Vim plugins...\n"
if [ -d ~/.vim/pack/plugins/start ];
    then
        cd ~/.vim/pack/plugins/start
    else
        printf "\nThe \"plugins\" directory does not exist, exiting.\n\n"
        exit 1
fi
for p in ${BUNDLES[@]};
do
    printf "git clone $p ..."
    git clone $p 1>/dev/null 2>&1
    if [ $? -gt '0' ];
    then
        printf "FAIL\n"
    else
        printf "Success\n"
    fi
done

# Get Thesaurus
if [ ! -f ~/.vim/thesaurus/mthesaur.txt ]
then
    curl -s https://www.gutenberg.org/files/3202/files/mthesaur.txt -o ~/.vim/thesaurus/mthesaur.txt
fi

printf "\nBuilding helptags...\n\n"
vim -c "helptags ALL" -c q
#if [ $? -gt '0' ];
#then
#    printf "Something went wrong, try opening vim and running :helptags ALL\n"

#fi;

printf "\n\nYou are Done. Happy editing...\n\n"
exit 0

