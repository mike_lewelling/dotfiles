" .vimrc

" Environment
"set nocompatible
set shell=bash
set runtimepath+=~/.vim/pack/plugins/start/LanguageClient-neovim

if has('multi_byte')
  if &termencoding is? ''
    let &termencoding = &encoding
  endif
  set encoding=utf-8                   " Enable display of UTF-8
  scriptencoding utf-8
  setglobal fileencoding=utf-8         " Enable file write in UTF-8
endif

" Vim Directory Setup
if has('win32') || has('win64')
    set runtimepath+=%USERPROFILE%\vimfiles
"" Setting up the backup directories for Windows (work) systems
    set backup
    set backupdir=%USERPROFILE%\vimfiles\misc\backups\\
    set directory=%USERPROFILE%\vimfiles\misc\swap\\
    set viewdir=%USERPROFILE%\vimfiles\misc\views\\
    set tags=%USERPROFILE%\vimfiles\misc\tags\\
    set spellfile+=%USERPROFILE%\vimfiles\spell\en.utf-8.add
    set thesaurus+=%USERPROFILE%\vimfiles\thesaurus\Rogets-pg10681.txt
    let g:WriteBackup_BackupDir='%USERPROFILE%\vimfiles\misc\backups\'
    set packpath+=%USERPROFILE%\vimfiles\pack
    set undofile
    set undodir=%USERPROFILE%\vimfiles\misc\backups\undo\\
else "Set up the directories for Unix/Linux systems
    set backup      " backups are nice ...
    set backupdir=$HOME/.vim/misc/backups//
    set directory=$HOME/.vim/misc/swap//
    set viewdir=$HOME/.vim/misc/views//
    set tags=$HOME/.vim/misc/tags//
    set spellfile+=$HOME/.vim/spell/en.utf-8.add
    set thesaurus+=$HOME/.vim/thesaurus/mthesaur.txt
    let g:WriteBackup_BackupDir='./.vim/misc/backups/'
    set packpath+=$HOME/.vim/pack
    set undofile
    set undodir=$HOME/.vim/misc/undo//
endif
"}

" General Settings / Various Options "{
filetype plugin on
filetype plugin indent on              " Enable auto-indent
set autoindent                         " indent same as the previous line
set autoread
set backspace=indent,start             " backspace for dummys
set breakindent
set clipboard=unnamedplus
set cursorline                         " highlight current line
set expandtab                          " tabs are spaces, not tabs
set formatoptions=crqnjl             " :h formatoptions for details
set history=30                         " Store more history (default is 20)
set hlsearch                           " highlight search terms
set incsearch                          " find as you type search
set linespace=0                        " No extra spaces between rows
set list listchars=tab:>.,trail:␣,nbsp:_,precedes:«,extends:»  " ,eol:¬  " Highlight whitespace
set noignorecase                       " case insensitive search
set number
set number relativenumber              " Enable show line numbers
set scrolljump=0                       " lines to scroll when cursor leaves screen
set scrolloff=0                        " minimum lines to keep above and below cursor
set shiftwidth=4                       " use indents of 4 spaces
set shortmess+=filmnrxoOtTc            " abbrev. of messages
set showcmd                            " show partial commands in status line
set showmatch                          " show matching brackets/parenthesis
set showmode                           " display the current mode
set sidescroll=5                       " Scroll sideways n characters at a time, rather than a screen at a time
set smartcase                          " case sensitive when uc present
set smartindent
set softtabstop=4
set splitbelow
set splitright
set tabpagemax=15                      " only show 15 tabs
set tabstop=4                          " set tab lenghth
set textwidth=0
set viewoptions=cursor,slash,unix      " better OS compatibility
set virtualedit=onemore                " allow for cursor beyond last character
set visualbell t_vb=$<100>         " set visualbell instead of audible bell
set winminheight=1                     " windows can be no less than 1 line high
set wrap                               " wrap long lines or do not
syntax on                              " syntax highlighting

" Spell Checking
setlocal spell spelllang=en_us,cy,es
set spellsuggest=5                     " show only 5 suggestions for misspelled words
set nospell                            " disable spell checking unless called
set complete+=kspell

" Completion
set wildmenu                           " show list instead of just completing
  set wildmode=longest,list            " set wildmode=longest,full
  set wildignore+=*.o,*.out,*.obj,.git,*.rbc,*.rbo,*.class,.svn,*.gem,*.pyc
  set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz
  set wildignore+=*.swp,*~,._*,*.jpg,*.png,*.gif,*.jpeg
  set wildignore+=*/.DS_Store,*/tmp/*
set completeopt+=longest,menuone
set completeopt=menu,menuone,noinsert
"}

"" Code folding options
"    set foldenable                    " auto fold code
"    set foldlevel=0 foldmethod=marker
"    set foldlevel=0 foldmethod=indent


" Auto-Commands
augroup templates                      " Enablee the use of templatesates
  au BufNewFile *.sh 0r ~/.vim/templates/bash.sh
augroup END

augroup autosize                       " Auto-resize splits when Vim gets resized
  au VimResized * wincmd =
augroup END

"augroup autosave                        " Auto-save all changed files upon loss of focus
"  au FocusLost * :wa
"augroup END

augroup viewsave
  let skiplist = [ 'gitcommit', 'help', 'terminal' ]
  au bufWinLeave * if index(skiplist, &ft) < 0 | silent! mkview | endif     " make vim save view (state)
  au bufWinEnter * if index(skiplist, &ft) < 0 | silent! loadview | endif   " make vim load view (state)
augroup END

augroup unsetpaste                     " Unset paste on InsertLeave
  au InsertLeave * silent! set nopaste
augroup END

augroup lastwin                        " Exit Vim if NERDTree is the only window left
  au bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
augroup END

augroup ts2
  au FileType yaml,nix setlocal ts=2 sts=2 sw=2 expandtab
augroup END

augroup whitespace                     " Do not remove whitespace for these file types
  au FileType ruby,javascript,perl,markdown,make let b:noStripWhitespace=1
  au FileType * call StripTrailingWhitespace()
augroup END

"augroup conceallevel                   " Enable conceal for the listed filetypes/disable for everything else
"  au FileType markdown set conceallevel=2
"  au FileType * set conceallevel=0
"augroup END

augroup keeptabs                       " Do not convert tabs to spaces for these filetypes
  au FileType make setlocal noexpandtab
augroup END

augroup workingDir                     " Change working directory to currently opened file
  au BufEnter * lcd %:p:h
augroup END

augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline
augroup END

augroup spellfiles
"  autocmd!
"  " Per file type spellfile
"  autocmd BufNewFile,BufRead *.md setlocal spellfile+=~/.vim/spell/markdown.utf-8.add
"
  if expand('%:t') =~# '.cy.'
      setlocal spellfile=$HOME/.vim/spell/cy.utf-8.add
  elseif expand('%:t') =~# '.es.'
      setlocal spellfile=$HOME/.vim/spell/es.utf-8.add
  else
      setlocal spellfile=$HOME/.vim/spell/en.utf-8.add
  endif

augroup END

augroup committext
    au FileType text,gitcommit setlocal spell
augroup END

" GUI settings "{
    if has('gui_running')
        " Set gvim titlebar/toolbar options
        set guioptions-=T              " remove toolbar
        set guioptions-=r              " remove right-hand scroll bar
        set guioptions-=L              " remove left-hand scroll bar
        set guioptions-=e              " use text-based tabline
"        set guioptions-=m              " remove the menubar
        " Set font to use in gvim
        if has('win32') || has('win64')
            set guifont=Consolas:h12
        else
            set guifont=Fira\ Code:h10
"            set guifont=Cascadia\ Code\ 14
            let g:neovide_cursor_animation_length = 0
        endif
    else
        if $TERM is# 'xterm-kitty' || $TERM is# 'alacritty' || $TERM is# 'gnome-terminal' || $TERM =~# '256color'
            let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
            let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
            set termguicolors          " Set term to use truecolor
        else
            set t_Co=256
        endif
    endif

" Source other customization files:
so $HOME/.vim/includes/plugin-variables.vim
so $HOME/.vim/includes/functions.vim
so $HOME/.vim/includes/key-mappings.vim
"so $HOME/.vim/includes/abbreviations.vim
so $HOME/.vim/includes/tabline.vim
"so $HOME/.vim/includes/simple-statusline.vim
so $HOME/.vim/includes/statusline.vim
so $HOME/.vim/includes/ALE-additions.vim

" I got lazy and started using the arrow keys
" I need to get back in the habit of only using hjkl
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

"Set tabline colors
"hi TabLine      ctermfg=fg  guifg=fg      ctermbg=242 guibg=#666666 cterm=none gui=none
"hi TabLineFill  ctermfg=fg  guifg=fg      ctermbg=237 guibg=#3a3a3a cterm=none gui=none
hi! link TabLineSel StatusLine
hi! link TabLine    StatusLineNC
hi! link TabLineFill    StatusLineNC

" Navigate the complete menu items like CTRL+n / CTRL+p would.
" inoremap <expr> j pumvisible() ? "<C-n>" : "j"
" inoremap <expr> k pumvisible() ? "<C-p>" : "k"
"
" " Select the complete menu item like CTRL+y would.
" inoremap <expr> l pumvisible() ? "<C-y>" : "l"
 inoremap <expr> <CR> pumvisible() ? "<C-y>" : "<CR>"
" inoremap <expr> <CR> pumvisible() ? "<C-y>" : "<C-g>u<CR>"
"
" " Cancel the complete menu item like CTRL+e would.
" inoremap <expr> h pumvisible() ? "<C-e>" : "h"

" Startify
" 'Most Recent Files' number
let g:startify_files_number = 6
" Add some bookmarks (most used documents)
let g:startify_bookmarks = [
\ '~\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1'
\ ]
" Update session automatically as you exit vim
let g:startify_session_persistence = 1
" Fancy custom header

if !has('nvim')
    packadd! git-lens.vim
    let g:startify_custom_header = [
    \ ' ',
    \ '    \  /  |  |\/|',
    \ '     \/   |  |  |   Editor',
    \ '  __________________________ ',
    \ ' ',
    \ ]
else
    packadd! blamer.nvim
    let g:startify_custom_header = [
    \ ' ',
    \ '       |\| [- () \/ | |\/|  ',
    \ ' ',
    \ ]
endif

" Set colorscheme
set background=dark
colorscheme night-owl

" End .vimrc file

