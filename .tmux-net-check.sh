#!/bin/bash -x
# Checks network connectivity with ping
# If timeout return the word 'Offline'

# Check for script lock
if [ -f ~/.tmux-net-check.lock ]; then
    exit
fi
# Lock script
date +'%Y%m%d@%H:%M' > ~/.tmux-net-check.lock

# Array of top 500 websites
sites=( \
www.google.com apple.com youtube.com cloudflare.com play.google.com microsoft.com support.google.com www.blogger.com wordpress.org maps.google.com youtu.be en.wikipedia.org mozilla.org linkedin.com docs.google.com europa.eu sites.google.com plus.google.com googleusercontent.com drive.google.com adobe.com accounts.google.com vimeo.com istockphoto.com bp.blogspot.com vk.com facebook.com t.me uol.com.br github.com bbc.co.uk whatsapp.com amazon.com creativecommons.org pt.wikipedia.org paypal.com netvibes.com google.co.jp google.de brandbucket.com feedburner.com news.google.com who.int bbc.com dropbox.com mail.ru msn.com live.com cnn.com imdb.com theguardian.com fr.wikipedia.org opera.com slideshare.net medium.com policies.google.com washingtonpost.com mail.google.com hugedomains.com es.wikipedia.org www.yahoo.com nih.gov terra.com.br w3.org reuters.com issuu.com forbes.com google.com.br dailymotion.com nytimes.com myspace.com line.me globo.com gstatic.com wikimedia.org www.weebly.com google.es developers.google.com change.org books.google.com abril.com.br de.wikipedia.org elmundo.es bloomberg.com plesk.com ok.ru wired.com twitter.com t.co booking.com dailymail.co.uk telegram.me mirror.co.uk google.ru time.com office.com marketingplatform.google.com independent.co.uk harvard.edu cpanel.com ig.com.br aboutads.info fb.com namecheap.com cbsnews.com myaccount.google.com nasa.gov aol.com networkadvertising.org pinterest.com telegraph.co.uk businessinsider.com gravatar.com jimdofree.com thesun.co.uk files.wordpress.com google.co.uk forms.gle translate.google.com draft.blogger.com picasaweb.google.com goo.gl cpanel.net abcnews.go.com android.com search.google.com steampowered.com mediafire.com huffingtonpost.com it.wikipedia.org bit.ly tinyurl.com google.fr dan.com amazon.de news.yahoo.com huffpost.com wsj.com amazon.co.uk cnet.com wa.me archive.org ebay.com google.pl rakuten.co.jp elpais.com un.org foxnews.com photos.google.com aliexpress.com get.google.com youronlinechoices.com soundcloud.com tools.google.com www.gov.uk buydomains.com enable-javascript.com list-manage.com samsung.com fandom.com usatoday.com amazon.co.jp cdc.gov scribd.com google.it netflix.com 4shared.com storage.googleapis.com whitehouse.gov themeforest.net id.wikipedia.org lg.com akamaihd.net google.ca nginx.com outlook.com quora.com google.com.tw php.net google.nl detik.com gizmodo.com rambler.ru lemonde.fr indiatimes.com ted.com researchgate.net picasa.google.com loc.gov bitly.com weibo.com stanford.edu imageshack.us wikihow.com disqus.com pbs.org afternic.com secureserver.net news.com.au nypost.com lefigaro.fr hp.com express.co.uk ja.wikipedia.org rottentomatoes.com zoom.us xinhuanet.com spiegel.de mozilla.com sciencemag.org playstation.com cbc.ca ietf.org instagram.com code.google.com eventbrite.com my.yahoo.com spotify.com usnews.com amazon.es disney.com metro.co.uk surveymonkey.com nationalgeographic.com addtoany.com rtve.es lavanguardia.com mega.nz goodreads.com google.co.in berkeley.edu oup.com yadi.sk pexels.com urbandictionary.com wiley.com mashable.com kickstarter.com tmz.com cnil.fr rt.com fb.me www.over-blog.com nature.com bloglovin.com clarin.com cnbc.com stackoverflow.com ovh.com icann.org netlify.app groups.google.com home.neustar nokia.com godaddy.com alibaba.com nbcnews.com wikia.com techcrunch.com discord.gg ru.wikipedia.org photobucket.com smh.com.au sapo.pt washington.edu amazon.fr discord.com adssettings.google.com yelp.com dw.com mysql.com arxiv.org nydailynews.com instructables.com theatlantic.com twitch.tv ziddu.com sedo.com nikkei.com amazon.it www.wix.com cornell.edu npr.org gofundme.com gnu.org welt.de latimes.com engadget.com target.com wp.com francetvinfo.fr allaboutcookies.org cambridge.org sciencedirect.com webmd.com orange.fr huawei.com yandex.ru rapidshare.com deezer.com umich.edu ft.com ria.ru alexa.com tripadvisor.com newyorker.com apache.org oracle.com mit.edu ytimg.com ea.com guardian.co.uk sfgate.com economist.com bing.com abc.es ox.ac.uk biglobe.ne.jp sciencedaily.com academia.edu gooyaabitemplates.com newsweek.com m.wikipedia.org theverge.com buzzfeed.com pixabay.com nginx.org about.com chicagotribune.com gmail.com hollywoodreporter.com ikea.com box.com bandcamp.com hatena.ne.jp shutterstock.com ibm.com pl.wikipedia.org thetimes.co.uk 20minutos.es lycos.com www.wikipedia.org britannica.com psychologytoday.com clickbank.net ipv4.google.com zendesk.com sky.com fda.gov shopify.com sputniknews.com yahoo.co.jp unesco.org google.co.id variety.com naver.com walmart.com ign.com amzn.to abc.net.au addthis.com www.gov.br hm.com finance.yahoo.com privacyshield.gov googleblog.com espn.com ggpht.com doubleclick.net squadhelp.com pinterest.co.uk statista.com e-monsite.com state.gov patreon.com sports.yahoo.com irs.gov example.com slate.com pewresearch.org pnas.org xing.com channel4.com thestar.com yale.edu dreamstime.com sina.com.cn kotaku.com politico.com howstuffworks.com dreniq.com upenn.edu vmware.com epa.gov yandex.com com.com liveinternet.ru windows.net utexas.edu indianexpress.com qq.com elsevier.com inc.com bp3.blogger.com insider.com boston.com rollingstone.com imgur.com ucoz.ru asus.com thedailybeast.com nba.com zeit.de techradar.com merriam-webster.com eonline.com digitaltrends.com scoop.it ovh.net standard.co.uk lifehacker.com depositfiles.com hbr.org calameo.com rbc.ru tiktok.com jstor.org fortune.com trustpilot.com fastcompany.com goo.ne.jp usgs.gov psu.edu video.google.com people.com scientificamerican.com en.wordpress.com amazon.ca ftc.gov freepik.com ebay.co.uk undeveloped.com thehindu.com vox.com pcmag.com skype.com ca.gov fifa.com ovh.co.uk usc.edu zdnet.com thehill.com vkontakte.ru wn.com springer.com nvidia.com foursquare.com princeton.edu canada.ca kinja.com bp1.blogger.com debian.org billboard.com last.fm parallels.com www.canalblog.com redhat.com gamestop.com offset.com histats.com thenextweb.com a8.net theglobeandmail.com thoughtco.com archives.gov asahi.com venturebeat.com adweek.com businesswire.com faz.net steamcommunity.com vice.com about.me sendspace.com sony.com mixcloud.com nhk.or.jp redbull.com namesilo.com moz.com search.yahoo.com coursera.org unicef.org cbslocal.com bp0.blogger.com ads.google.com ctvnews.ca ubuntu.com wiktionary.org weforum.org )
#sites=(10.0.5.3 10.0.24.8 apple.com)
site_count=${#sites[@]}

function pinger() {
    i=$((RANDOM % $site_count))
    test=$(ping -c2 ${sites[$i]} >/dev/null 2>>~/.tmux-net-check.lock; echo $?)
    return $test;
}

pinger

while [ "$test" -gt 1 ];
do
    fc=$((fc+1))
    printf "$fc" >> ~/.tmux-net-check.lock
    if [ $fc -gt 1 ]; then
        printf "#[fg=brightred,bold]OFFLINE   #[fg=default]"
    fi
    sleep 30
    pinger
done
fc=0
rm ~/.tmux-net-check.lock
