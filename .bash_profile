
## Check if starting bash in TMUX and source .bashrc if true
if [ -n "$TERM_PROGRAM" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
    . "$HOME/.bashrc"
    fi
fi
